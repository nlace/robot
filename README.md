# NRMC 2017 Robot #

I realized there was no version control for the robot, so I uploaded it here. 

### What is this repository for? ###

To share SolidWorks files between members of the mechanical team for the project.

To allow said team members to branch the main design and suggest other approaches without messing up the main design.